package com.technoelevate.pdf.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PdfDTO {

private String title;
private String content;
private String header;
}

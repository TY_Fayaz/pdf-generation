package com.technoelevate.pdf.controller;

import java.io.ByteArrayInputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.technoelevate.pdf.dto.PdfDTO;
import com.technoelevate.pdf.service.PdfService;

@RestController
public class PdfController {
	@Autowired
	private PdfService service;

	@GetMapping("/createPdf")
	public ResponseEntity<InputStreamResource> createPdf(@RequestBody PdfDTO pdfDto) {
		ByteArrayInputStream pdf = service.createPdf(pdfDto);
		//in this header we give the information about the file
		HttpHeaders httpHeaders = new HttpHeaders();
		//header name and filename
		httpHeaders.add("India", "inline;file=india.pdf");
		return ResponseEntity.ok().headers(httpHeaders).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(pdf));
	}

}

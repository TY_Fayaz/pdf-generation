package com.technoelevate.pdf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Pdf {
private String title;
private String content; 
private String header;
}

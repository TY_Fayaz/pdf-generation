package com.technoelevate.pdf.service;

import java.io.ByteArrayInputStream;

import com.technoelevate.pdf.dto.PdfDTO;

public interface PdfService {

	public ByteArrayInputStream createPdf(PdfDTO pdfDto);

}

package com.technoelevate.pdf.service;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Header;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;
import com.technoelevate.pdf.dto.PdfDTO;

@Service
public class PdfServiceImpl implements PdfService {

	private Logger logger = LoggerFactory.getLogger(PdfServiceImpl.class);

	
	public ByteArrayInputStream createPdf(PdfDTO pdfDto) {
		logger.info("creating pdf started ");

		// to write the data
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		// to create pdf we need  document
		Document document = new Document(PageSize.A4);

		//the data which we write in document will be available in out
		PdfWriter.getInstance(document, out);
		
		HeaderFooter header = new HeaderFooter(new Phrase(pdfDto.getHeader()),false);
		
		  header.setAlignment(Element.ALIGN_CENTER);
		  header.setBorder(Rectangle.BOTTOM);
		  
		  document.setHeader(header);
		

		HeaderFooter footer = new HeaderFooter(true, new Phrase(" page"));
		footer.setAlignment(Element.ALIGN_CENTER);
		footer.setBorderWidthBottom(0);
		document.setFooter(footer);

		document.open();

		Font titleFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 28,Color.GREEN);

		Paragraph titleparagraph = new Paragraph(pdfDto.getTitle(), titleFont);
		titleparagraph.setAlignment(Element.ALIGN_CENTER);
		document.add(titleparagraph);

		Font paraFont = FontFactory.getFont(FontFactory.TIMES_ITALIC, 18,Color.BLACK);
		Paragraph paragraph = new Paragraph(pdfDto.getContent(),paraFont);
		// chunk means a piece of data
		paragraph.add(new Chunk("I love my country"));
		document.add(paragraph);

		document.close();
		//if we return ByteArrayOutputStream the user cannot read the date so we return in input stream
		return new ByteArrayInputStream(out.toByteArray());
	}

}
